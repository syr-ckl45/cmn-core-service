package com.cokreates.core;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CklServiceInterface<T extends MasterDTO> {
	
	
	public void insert(T dto);
	public Page<T> search(Pageable pageable);
	public Optional<? extends T> getDTO(UUID id);
	public void update(UUID id, T dto);
	public void delete(UUID id);
	

}
