package com.cokreates.core;

import java.util.UUID;

public abstract class MasterService<T extends MasterDTO> implements CklServiceInterface<T> {

    protected CklDataRepository<T, UUID> cklDataRepository;

    @Override
    public void insert(T dto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(UUID id, T dto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(UUID id) {
        throw new UnsupportedOperationException();
    }

}
