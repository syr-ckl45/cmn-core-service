package com.cokreates.core;

import java.util.Calendar;
import java.util.Date;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ResponseData {

	private int status;
	private String message;
	private Object errors;
	private Object data;
	private Object options;
	private Date timestamp = Calendar.getInstance().getTime();

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public static synchronized ResponseData success(Object data) {
		ResponseData obj = new ResponseData();
		obj.status = HttpStatus.OK.value();
		obj.data = data;
		return obj;
	}

	public static synchronized ResponseData error(int status, Object error, String msg) {
		ResponseData obj = new ResponseData();
		obj.status = status;
		obj.errors = error;
		obj.message = msg;
		return obj;
	}

	private ResponseData() {
	}


}