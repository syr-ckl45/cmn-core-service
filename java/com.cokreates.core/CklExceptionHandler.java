package com.cokreates.core;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class CklExceptionHandler{

//	@ResponseStatus(HttpStatus.BAD_REQUEST)
//	@ExceptionHandler(BindException.class)
//	@ResponseBody
//	public ResponseEntity<ResponseData> handleValidationException(BindException ex) {
//		Map<String, String> errors = new HashMap<String, String>();
//		ex.getBindingResult().getAllErrors().forEach(error -> {
//			errors.put(((FieldError) error).getField(), error.getDefaultMessage());
//		});
//		return new ResponseEntity<ResponseData>(ResponseData.setErrorResponse(errors, "Validation failed"), HttpStatus.BAD_REQUEST);
//	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(BindException.class)
	@ResponseBody
	public Map<String, String> handleValidationException(BindException ex) {
		Map<String, String> errors = new HashMap<String, String>();
		ex.getBindingResult().getAllErrors().forEach(error -> {
			errors.put(((FieldError) error).getField(), error.getDefaultMessage());
		});
		return errors;
	}

}
