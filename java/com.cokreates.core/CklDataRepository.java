package com.cokreates.core;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface CklDataRepository<T, ID> extends PagingAndSortingRepository<T, ID> {

}
