package com.cokreates.core;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.UUID;

public interface CklRestController<T extends MasterDTO> {


    public ResponseEntity<ResponseData> search(Pageable pageable);

    public ResponseEntity<ResponseData> getById(@PathVariable UUID id);

    public ResponseEntity<ResponseData> create(@RequestBody T dto);

    public ResponseEntity<ResponseData> update(@PathVariable UUID id, @RequestBody T dto);

    public ResponseEntity<ResponseData> delete(@PathVariable UUID id);

}
