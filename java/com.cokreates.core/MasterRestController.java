package com.cokreates.core;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Component
public abstract class MasterRestController<T extends MasterDTO> implements CklRestController<T> {

    protected CklServiceInterface<T> cklServiceInterface;

    @Override
    public ResponseEntity<ResponseData> create(T dto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ResponseEntity<ResponseData> update(UUID id, T dto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ResponseEntity<ResponseData> delete(UUID id) {
        throw new UnsupportedOperationException();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BindException.class)
    @ResponseBody
    public ResponseEntity<ResponseData> handleValidationException(BindException ex) {
        Map<String, String> errors = new HashMap<String, String>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            errors.put(((FieldError) error).getField(), error.getDefaultMessage());
        });
        return new ResponseEntity<ResponseData>(ResponseData.error(HttpStatus.BAD_REQUEST.value(), errors, "Validation failed"), HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ResponseEntity<ResponseData> methodArgumentNotValidExceptionEntity(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<String, String>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            errors.put(((FieldError) error).getField(), error.getDefaultMessage());
        });
        return new ResponseEntity<ResponseData>(ResponseData.error(HttpStatus.BAD_REQUEST.value(), errors, "Validation failed"), HttpStatus.BAD_REQUEST);
    }

}
